from pymongo import MongoClient

client = MongoClient()
db = client.mydb
collection = db.mark2

new_posts = [{"Name": "Rohan",
               "Marks": 10},
              {"Name": "Adam",
               "Marks": 11},
            {"Name": "Sam",
               "Marks": 12},
            {"Name": "Rock",
               "Marks": 11},
            {"Name": "Tom",
               "Marks": 10},
            {"Name": "John",
               "Marks": 9},
            {"Name": "Ankit",
               "Marks": 8},
            {"Name": "Lemon",
               "Marks": 12}]

collection.insert_many(new_posts)

for post in collection.find():
    print(post)

collection.drop()
client.close()
