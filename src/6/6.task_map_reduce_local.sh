#!/usr/bin/env bash

TASK_DIR='/Users/Rohan/Desktop/Cl/ST/fork/dst-stu/src/6'
swPth='/Users/Rohan/Desktop/Cl/ST/fork/dst-stu/src'
inpDir='/Users/Rohan/Desktop/Cl/ST/fork/dst-stu/d/mr/tf-idf'

cat $inpDir/* | python "$TASK_DIR/mapper.py" | sort -k1,1 | python "$TASK_DIR/reducer.py"  | tee "$TASK_DIR/out.tsv"
