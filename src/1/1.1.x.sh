#!/usr/bin/env bash

TASK_DIR='/Users/Rohan/Desktop/Cl/ST/fork/dst-stu/src/1'
DATA_DIR='/Users/Rohan/Desktop/Cl/ST/fork/dst-stu/d'
PROGRAM_DIR='/Users/Rohan/Desktop/Cl/ST/fork/dst-stu/src'

curl -o "$TASK_DIR/data-2015-03-11T00-00-00-structure-2015-03-11T00-00-00.csv" "http://data.gov.ru/sites/default/files/opendata/7107542055-ioszpro/data-2015-03-11T00-00-00-structure-2015-03-11T00-00-00.csv"

curl -o "$TASK_DIR/hads2013n_ASCII.zip" "https://www.huduser.gov/portal/datasets/hads/hads2013n_ASCII.zip"

unzip "$TASK_DIR/hads2013n_ASCII.zip"

tail "$DATA_DIR/l1.log"

head "$DATA_DIR/l1.log"

mkdir "$DATA_DIR/log"
mkdir "$DATA_DIR/log/l1"

split -b 4k "$DATA_DIR/l1.log" "$DATA_DIR/log/l1/segment"

wc "$DATA_DIR/l1.log"
cat "$DATA_DIR/l1.log" | gsed -r 's/[     ]+/\n/g' | wc -l

cat "$DATA_DIR/tre.csv" | awk -f "$PROGRAM_DIR/csv-to-tree.awk" > "$DATA_DIR/tre.dot" && dot -Tsvg -Kdot -o"$DATA_DIR/tre.svg" "$DATA_DIR/tre.dot" #&& gweniew "$DATA_DIR/tre.svg"
