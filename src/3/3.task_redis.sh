TASK_DIR='/Users/Rohan/Desktop/Cl/ST/fork/dst-stu/src/3'

redis-server "$TASK_DIR/redis.conf" --daemonize yes

redis-cli -n 0 set my-address "Novosibirsk, Akademgorodok"
redis-cli -n 0 set my-country "India"
redis-cli -n 0 set my-name "Rohan Kumar Rathore"
redis-cli -n 0 set my-education "Big Data Analytics & Artificial Intelligence"

redis-cli -n 0 mget my-address my-country my-name my-education
