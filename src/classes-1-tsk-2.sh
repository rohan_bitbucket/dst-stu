#!/usr/bin/env bash

DATA_DIR='/Users/Rohan/Desktop/Cl/ST/fork/dst-stu/d'
PROGRAM_DIR='/Users/Rohan/Desktop/Cl/ST/fork/dst-stu/src'

cat "$DATA_DIR/tre.csv" | awk -f "$PROGRAM_DIR/csv-to-tree.awk" > "$DATA_DIR/tre.dot" && dot -Tsvg -Kdot -o"$DATA_DIR/tre.svg" "$DATA_DIR/tre.dot" #&& gweniew "$DATA_DIR/tre.svg"

cat "$DATA_DIR/tre.csv" | awk -f "$PROGRAM_DIR/csv-to-tree.awk" > "$DATA_DIR/tre.dot" && dot -Tsvg -Kneato -o"$DATA_DIR/tre.svg" "$DATA_DIR/tre.dot" #&& gweniew "$DATA_DIR/tre.svg"
