#!/usr/bin/env bash

echo "hello"

DATA_DIR='/Users/Rohan/Desktop/Cl/ST/fork/dst-stu/d'

head -n 5 "$DATA_DIR/f1.tsv"

cat "$DATA_DIR/f1.tsv" > "$DATA_DIR/f1-out.tsv"

cat "$DATA_DIR/f1.tsv" |tee "$DATA_DIR/f1-out.tsv"

cat "$DATA_DIR/f1.tsv" | gsed -r 's/[     ]+/\n/g' | wc -l |tee "$DATA_DIR/f1-out.tsv"

cat "$DATA_DIR/f1.tsv" | gsed -r 's/[     ]+/\n/g' | sort | uniq -c | sort -r | tee "$DATA_DIR/f1-out.tsv"
